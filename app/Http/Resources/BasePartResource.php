<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BasePartResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'description' => $this->description,
            'car_region' => $this->car_region,
            'ml_variation' => $this->ml_variation ? true : false,
            'technical_description_new' => $this->technical_description_new,
            'technical_description_old' => $this->technical_description_old,
            'clas_indaia_new' => $this->clas_indaia_new,
            'clas_indaia_old' => $this->clas_indaia_old,
            'freight' => $this->freight,
            'ml_category' => $this->ml_category
        ];
    }
}
