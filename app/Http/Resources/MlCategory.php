<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MlCategory extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'ml_code' => $this->ml_code,
        ];
    }
}
