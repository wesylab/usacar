<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Providers\FileServiceProvider;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Mail;
use App\Mail\{ UserCreated };
use App\{ User, Role };

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->path = "storage/users/";
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function insert(Request $request)
    {
        try{
            $this->validate($request, [
                'name' => 'required|string',
                'email' => 'required|string',
            ]);
            
            // if(!$request->hasFile('image'))
            //     return response()->json(['error' => 'A imagem é obrigatória'], 400);

            $user = new User();
            $user->name = $request->name;
            $user->email = $request->email;
            $password = $user->newPassword();

            if($request->user()->hasRole('master')){
                $role = Role::where(['name' => 'admin'])->first();
            }elseif($request->user()->hasRole('admin')){
                $role = Role::where(['name' => 'employee'])->first();
            }else{
                return response()->json(["error" => "Você não tem permissão para adicionar usuários"], 401);
            }

            // $fileService = new FileServiceProvider();
            // $response = $fileService->upload($request, $this->path);

            // $user->file()->associate($response['file']);

            $user->save();
            $user->roles()->attach($role);

            Mail::to($user->email)->send(new UserCreated($user, $password));

            // $user->photo();
            return response()->json($user->toArray(), 201);
        } catch (\Throwable $th) {
            return response()->json(["error" => $th->getMessage()], 500);
        }
    }

    public function update(Request $request, $id)
    {
        try{
            $user = User::find($id);
            $user->name = $request->name ?? $user->name;
            $user->email = $request->email ?? $user->email;

            // if($request->hasFile('image'))
            // {
            //     $fileService = new FileServiceProvider();
            //     $response = $fileService->delete($user->file);

            //     $fileService = new FileServiceProvider();
            //     $response = $fileService->upload($request, $this->path);
            //     $user->file()->associate($response['file']);
            // }

            $user->save();
            // $user->photo();
            return response()->json($user->toArray(), 201);
        } catch (\Throwable $th) {
            return response()->json(["error" => $th->getMessage()], 500);
        }
    }

    public function all(Request $request)
    {
        try {
            $pagination = --$request->pagination ?? 0;
            $limit = $request->limit ?? 10;
            $offset = $pagination * $limit;

            if($request->user()->hasRole('master')){
                $users = User::whereHas('roles', function (Builder $query) {
                    $query->where('name', 'admin');
                });
            }elseif($request->user()->hasRole('admin')){
                $users = User::whereHas('roles', function (Builder $query) {
                    $query->where('name', 'employee');
                });
            }else{
                return response()->json(["error" => "Você não tem permissão para adicionar usuários"], 401);
            }

            $users = $users->skip($offset * $limit)->take($limit)->get();

            // foreach($users as $user)
            // {
            //     $user->photo();
            // }

            return response()->json($users->toArray(), 200);

        } catch (\Throwable $th) {
            return response()->json(["error" => $th->getMessage()], 500);
        }
    }

    public function get(Request $request, $id)
    {
        try {

            $user = User::find($id);
            // $user->photo();
            return response()->json($user->toArray(), 200);

        } catch (\Throwable $th) {
            return response()->json(["error" => $th->getMessage()], 500);
        }
    }

    public function delete(Request $request, $id)
    {
        try {
            $user = user::find($id);

            // $fileService = new FileServiceProvider();
            // $response = $fileService->delete($user->file);

            $user->delete();
            return response()->json(['message' => 'Deletado com sucesso'], 200);

        } catch (\Throwable $th) {
            return response()->json(["error" => $th->getMessage()], 500);
        }
    }

    public function changePassword(Request $request, $id)
    {
        $user = User::find($id);
        
        if($user == null)
            return response()->json(["error" => "Usuário não encontrado"], 401);

        if(!password_verify($request->old_password, $user->password))
            return response()->json(["error" => "Senha antiga inválida"], 401);

        $pass = $user->newPassword($request->new_password);
        
        $user->save();
        
        return response()->json(["message" => 'Senha atualizada com sucesso.'], 200);
    }

    public function resetPassword(Request $request, $id)
    {
        $user = User::find($id);
        
        if($user == null)
            return response()->json(["error" => "Usuário não encontrado"], 401);

        $pass = $user->newPassword();
        
        $user->save();
        
        // Mail::to($user->email)->send(new ResetPassword($user, $pass));
        
        return response()->json(["message" => 'Senha atualizada com sucesso.'], 200);
    }

}
