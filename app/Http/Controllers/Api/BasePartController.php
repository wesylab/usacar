<?php


namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\{ BasePart, MlCategory };
use App\Http\Resources\BasePartResource;
use App\Providers\MlServiceProvider;

class BasePartController extends Controller
{

    public function insert(Request $request)
    {
        try{
            $this->validate($request, [
                'description' => 'required|string',
                'car_region' => 'required|string',
            ]);

            $base_part                            = new BasePart();
            $base_part->car_region                = $request->car_region;
            $base_part->description               = $request->description;
            $base_part->ml_variation              = $request->ml_variation ?? null;
            $base_part->clas_indaia_new           = $request->clas_indaia_new ?? null;
            $base_part->clas_indaia_old           = $request->clas_indaia_old ?? null;
            $base_part->technical_description_old = $request->technical_description_old ?? null;
            $base_part->technical_description_new = $request->technical_description_new ?? null;
            
            if($request->ml_category_id)
            {
                $ml_category = MlCategory::findOrFail($request->ml_category_id);
                $base_part->ml_category()->associate($ml_category);
            }

            $base_part->save();

            $base_part->saveFreight($request);

            return response()->json(new BasePartResource($base_part), 201);
        } catch (\Throwable $th) {
            return response()->json(["error" => $th->getMessage()], 500);
        }
    }

    public function update(Request $request, $id)
    {
        try{
            $base_part              = BasePart::find($id);
            $base_part->description = $request->description ?? $base_part->description;
            
            $base_part->car_region = $request->car_region ?? $base_part->car_region;
            $base_part->description = $request->description ?? $base_part->description;
            $base_part->ml_variation = $request->ml_variation ?? $base_part->ml_variation;
            $base_part->clas_indaia_new = $request->clas_indaia_new ?? $base_part->clas_indaia_new;
            $base_part->clas_indaia_old = $request->clas_indaia_old ?? $base_part->clas_indaia_old;
            $base_part->technical_description_old = $request->technical_description_old ?? $base_part->technical_description_old;
            $base_part->technical_description_new = $request->technical_description_new ?? $base_part->technical_description_new;
            
            if($request->ml_category_id)
            {
                $ml_category = MlCategory::findOrFail($request->ml_category_id);
                $base_part->ml_category()->associate($ml_category);
            }

            $base_part->save();
            $base_part->saveFreight($request);
            return response()->json(new BasePartResource($base_part), 201);
        } catch (\Throwable $th) {
            return response()->json(["error" => $th->getMessage()], 500);
        }
    }

    public function all(Request $request)
    {
        try {
            $pagination = --$request->pagination ?? 0;
            $limit      = $request->limit ?? 10;
            $order      = $request->order ?? "asc";
            $offset     = $pagination * $limit;

            $base_parts = BasePart::skip($offset * $limit)->take($limit)->orderBy('description', $order)->get();

            return response()->json(BasePartResource::collection($base_parts), 200);

        } catch (\Throwable $th) {
            return response()->json(["error" => $th->getMessage()], 500);
        }
    }

    public function get(Request $request, $id)
    {
        try {

            $base_part = BasePart::findOrFail($id);
            return response()->json(new BasePartResource($base_part), 200);

        } catch (\Throwable $th) {
            return response()->json(["error" => $th->getMessage()], 500);
        }
    }

    public function delete(Request $request, $id)
    {
        try {
            $base_part = BasePart::findOrFail($id);
            $base_part->delete();
            return response()->json(['message' => 'Deletado com sucesso'], 200);

        } catch (\Throwable $th) {
            return response()->json(["error" => $th->getMessage()], 500);
        }
    }

    public function teste(Request $request)
    {
        $ml = new MlServiceProvider();
        $ml->categories();
    }

}
