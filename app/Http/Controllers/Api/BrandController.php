<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Providers\FileServiceProvider;
use App\Models\Brand;

class BrandController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->path = "storage/brands/";
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function insert(Request $request)
    {
        try{
            $this->validate($request, [
                'name' => 'required|string',
            ]);
            
            if(!$request->hasFile('image'))
                return response()->json(['error' => 'A imagem é obrigatória'], 400);

            $brand = new Brand();
            $brand->name = $request->name;

            $fileService = new FileServiceProvider();
            $response = $fileService->upload($request, $this->path);

            $brand->file()->associate($response['file']);

            $brand->save();

            $brand->logo();
            return response()->json($brand->toArray(), 201);
        } catch (\Throwable $th) {
            return response()->json(["error" => $th->getMessage()], 500);
        }
    }

    public function update(Request $request, $id)
    {
        try{
            $brand = Brand::find($id);
            $brand->name = $request->name ?? $brand->name;

            if($request->hasFile('image'))
            {
                $fileService = new FileServiceProvider();
                if($brand->file != null)
                    $response = $fileService->delete($brand->file);

                $fileService = new FileServiceProvider();
                $response = $fileService->upload($request, $this->path);
                $brand->file()->associate($response['file']);
            }

            $brand->save();
            $brand->logo();
            return response()->json($brand->toArray(), 201);
        } catch (\Throwable $th) {
            return response()->json(["error" => $th->getMessage()], 500);
        }
    }

    public function all(Request $request)
    {
        try {
            $pagination = --$request->pagination ?? 0;
            $limit = $request->limit ?? 10;
            $order = $request->order ?? "asc";
            $offset = $pagination * $limit;

            $brands = Brand::skip($offset * $limit)->take($limit)->orderBy('name', $order)->get();

            foreach($brands as $brand)
            {
                $brand->logo();
            }

            return response()->json($brands->toArray(), 200);

        } catch (\Throwable $th) {
            return response()->json(["error" => $th->getMessage()], 500);
        }
    }

    public function get(Request $request, $id)
    {
        try {

            $brand = Brand::find($id);
            $brand->logo();
            return response()->json($brand->toArray(), 200);

        } catch (\Throwable $th) {
            return response()->json(["error" => $th->getMessage()], 500);
        }
    }

    public function delete(Request $request, $id)
    {
        try {
            $brand = Brand::find($id);

            $fileService = new FileServiceProvider();
            $response = $fileService->delete($brand->file);

            $brand->delete();
            return response()->json(['message' => 'Deletado com sucesso'], 200);

        } catch (\Throwable $th) {
            return response()->json(["error" => $th->getMessage()], 500);
        }
    }

}
