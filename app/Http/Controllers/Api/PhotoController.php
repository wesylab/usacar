<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Providers\PhotoServiceProvider;
use App\Models\{ Photo, File, Angle };

class PhotoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function delete(Request $request, $id)
    {
        try {
            $photo = Photo::find($id);
            if($photo == null)
                return response()->json(['message' => 'Foto não encontrada.'], 400);

            $photoService = new PhotoServiceProvider();
            $photoService->delete($photo);

            return response()->json(['message' => 'Deletado com sucesso.'], 200);

        } catch (\Throwable $th) {
            return response()->json(["error" => $th->getMessage()], 500);
        }
    }

    public function linkToAngle(Request $request, $id)
    {
        try {

            if(!$request->angle_id)
                return response()->json(['message' => 'Ângulo não informado.'], 400);

            $photo = Photo::find($id);
            if($photo == null)
                return response()->json(['message' => 'Foto não encontrada.'], 400);
        
            $angle = Angle::find($request->angle_id);
            if($angle == null)
                return response()->json(['message' => 'Ângulo não encontrado.'], 400);

            $photo->angle()->associate($angle);
        
            return response()->json(['message' => 'Vinculo realizado com sucesso.'], 200);

        } catch (\Throwable $th) {
            return response()->json(["error" => $th->getMessage()], 500);
        }

    }

}
