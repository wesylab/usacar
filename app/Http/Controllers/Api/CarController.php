<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Providers\PhotoServiceProvider;
use App\Models\{ Car, Brand, Photo, File };

class CarController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->path = "storage/brands/";
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    
    public function insert(Request $request)
    {
        try{
            $car = new Car();
            $car->name = $request->name;

            $brand = Brand::find($request->brand_id);
            $car->brand()->associate($brand);
            
            $car->save();
            $car->cover = "https://cdn4.iconfinder.com/data/icons/car-silhouettes/1000/sedan-512.png";
            return response()->json($car->toArray(), 201);
        } catch (\Throwable $th) {
            return response()->json(["error" => $th->getMessage()], 500);
        }
    }

    public function update(Request $request, $id)
    {
        try{
            $car = Car::find($id);
            $car->name = $request->name ?? $car->name;

            if(isset($request->brand_id)){
                $brand = Brand::find($request->brand_id);
                $car->brand()->associate($brand);
            }
            $car->brand;            
            $car->save();
            $car->cover();
            return response()->json($car->toArray(), 201);
        } catch (\Throwable $th) {
            return response()->json(["error" => $th->getMessage()], 500);
        }
    }

    public function all(Request $request)
    {
        try {
            $pagination = --$request->pagination ?? 0;
            $limit = $request->limit ?? 10;
            $order = $request->order ?? "asc";
            $offset = $pagination * $limit;

            $brand = $request->brand ?? null;

            if($brand != null)
                $cars = Car::where(['brand_id' => $brand])->skip($offset * $limit)->take($limit)->orderBy('name', $order)->get();
            else  
                $cars = Car::skip($offset * $limit)->take($limit)->orderBy('name', $order)->get();

            foreach ($cars as $car) {
                $car->brand;
                $car->cover();
            }

            return response()->json($cars->toArray(), 200);

        } catch (\Throwable $th) {
            return response()->json(["error" => $th->getMessage()], 500);
        }
    }

    public function get(Request $request, $id)
    {
        try {

            $car = Car::find($id);
            $car->brand;
            $car->cover();
            $car->versions_year = $car->yearsVersion();
            return response()->json($car->toArray(), 200);

        } catch (\Throwable $th) {
            return response()->json(["error" => $th->getMessage()], 500);
        }
    }

    public function yearsVersion(Request $request, $id)
    {
        try {
            $car = Car::find($id);
            $car->yearsVersion();
            return response()->json($car->yearsVersion, 200);

        } catch (\Throwable $th) {
            return response()->json(["error" => $th->getMessage()], 500);
        }
    }

    public function delete(Request $request, $id)
    {
        try {
            Car::destroy($id);
            return response()->json(['message' => 'Deletado com sucesso'], 200);

        } catch (\Throwable $th) {
            return response()->json(["error" => $th->getMessage()], 500);
        }
    }

    public function photos(Request $request, $id)
    {
        $car = Car::find($id);

        if($car == null)
            return response()->json(["error" => "Modelo não encontrado."], 400);

        foreach($car->photos as $photo)
        {
            $photo->url();
        }

        return response()->json($car->photos, 200);
    }

    public function upload(Request $request, $id)
    {
        $car = Car::find($id);

        if($car == null)
            return response()->json(["error" => "Modelo não encontrado."], 400);

        $photoService = new PhotoServiceProvider();
        $photos = $photoService->upload($request, $car);

        foreach($photos as $photo)
        {
            $photo->url();
        }

        return response()->json($photos, 200);
    }

}
