<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\{ Subversion, Version };
use App\Http\Controllers\Controller;
use App\Providers\PhotoServiceProvider;

class SubversionController extends Controller
{

    public function photos(Request $request, $id)
    {
        $subversion = Subversion::find($id);

        if($subversion == null)
            return response()->json(["error" => "Subversão não encontrado."], 400);

        foreach($subversion->photos as $photo)
        {
            $photo->url();
        }

        return response()->json($subversion->photos, 200);
    }

    public function upload(Request $request, $id)
    {
        $subversion = Subversion::find($id);

        if($subversion == null)
            return response()->json(["error" => "Subversão não encontrado."], 400);

        $photoService = new PhotoServiceProvider();
        $photos = $photoService->upload($request, $subversion);

        foreach($photos as $photo)
        {
            $photo->url();
        }

        return response()->json($photos, 200);
    }


    public function all(Request $req){
        try{
            $pagination = $req->pagination ?? 0;
            $limit = $req->limit ?? 10;
            $offset = $pagination * $limit;
            $order = $req->order ?? 'asc';
            $version_id = $req->version_id ?? null;
            $sbvs = Subversion::where('version_id', $version_id)->skip($offset * $limit)->take($limit)->orderBy('name', $order)->get();
            foreach($sbvs as $sv){
                $sv->url = $sv->cover();
                $sv->years();
            }
            return response()->json($sbvs, 200);
        }catch(\Throwable $th){
            return response()->json(["error" => $th->getMessage()], 500);
        }
    }

    public function get($id){
        try{
            $sv = Subversion::find($id);
            if($sv == null){
                return response()->json(['msg' => 'Subversion not found'], 404);
            }else{
                /*$sv->urls = [];
                foreach($sv->photos as $photo)
                {
                    $sv->urls[] = $photo->getUrl();
                }*/
                $sv->years();
                $sv->url = $sv->cover();
                return response()->json($sv, 200);
            }

        }catch(\Throwable $th){
            return response()->json(["error" => $th->getMessage()], 500);
        }
    }

    public function insert(Request $req){
        try{
            $this->validate($req, [
                'name' => 'required|string',
                'version_id' => 'required|integer',
            ]);

            $v_id = $req->version_id;
            $version = Version::findOrFail($v_id);
//            if($version == null){
//                return response()->json(["error" => "Version id ".$v_id." not found"], 404);
//            }

            $sv = new Subversion();
            $sv->name = $req->name;

            $sv->version()->associate($version);

            $sv->save();

            $years = $req->years;
            $sv->saveYears($years);
            $sv->years();
            $sv->url = "https://cdn4.iconfinder.com/data/icons/car-silhouettes/1000/sedan-512.png";
            
            return response()->json($sv, 201);

        }catch(\Throwable $th){
            return response()->json(["error" => $th->getMessage()], 500);
        }
    }

    public function update(Request $req, $id){
        try{
            $sv = Subversion::find($id);
            if($sv == null){
                return response()->json(['error' => 'Subversion id '.$id.' not found', 400]);
            }
            $sv->name = $req->name ?? $sv->name;
            if(isset($req->version_id) and $req->version_id != null){
                $version = Version::find($req->version_id);
                if($version == null){
                    return response()->json(['error' => 'Version id '.$req->version_id.' not found'], 404);
                }
                $sv->version()->associate($version);
            }
            $sv->saveYears($req->years);
            $sv->save();
            $sv->years();
            $sv->url = $sv->cover();
            return response()->json($sv, 200);
        }catch(\Throwable $th){
            return response()->json(["error" => $th->getMessage()], 500);
        }
    }

    public function delete($id){
        try {
            Subversion::destroy($id);
            return response()->json(['message' => 'Deletado com sucesso'], 200);
        } catch (\Throwable $th) {
            return response()->json(["error" => $th->getMessage()], 500);
        }
    }
}
