<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use App\Providers\PhotoServiceProvider;
use App\Models\{ Car, Version, Photo, File, VersionYear };

class VersionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function insert(Request $request)
    {
        try{
            $version = new Version();
            $version->name = $request->name;
            $years = $request->years;

            $car = Car::find($request->car_id);
            $version->car()->associate($car);

            $version->save();

            $version->saveYears($years);

            $version->cover();
            $version->years();
            return response()->json($version->toArray(), 201);
        } catch (\Throwable $th) {
            return response()->json(["error" => $th->getMessage()], 500);
        }
    }

    public function update(Request $request, $id)
    {
        try{

            $version = Version::find($id);
            $version->name = $request->name ?? $version->name;
            $years = $request->years ?? $version->year;

            if(isset($request->car_id)){
                $car = Car::find($request->car_id);
                $version->car()->associate($car);
            }
            $version->car;
            $version->save();

            if($years)
                $version->saveYears($years);

            $version->cover();
            $version->years();
            return response()->json($version->toArray(), 201);
        } catch (\Throwable $th) {
            return response()->json(["error" => $th->getMessage()], 500);
        }
    }

    public function all(Request $request)
    {
        try {
            $pagination = --$request->pagination ?? 0;
            $limit = $request->limit ?? 10;
            $offset = $pagination * $limit;

            $car = $request->car ?? null;
            $year = $request->year ?? null;

            $versions = $this->filter($request);

            foreach ($versions as $version) {
                $version->car;
                $version->cover();
                $version->years();
            }
            return response()->json($versions->toArray(), 200);

        } catch (\Throwable $th) {
            return response()->json(["error" => $th->getMessage()], 500);
        }
    }

    public function get(Request $request, $id)
    {
        try {

            $version = Version::find($id);
            $version->car;
            $version->cover();
            $version->years();
            return response()->json($version->toArray(), 200);

        } catch (\Throwable $th) {
            return response()->json(["error" => $th->getMessage()], 500);
        }
    }

    public function delete(Request $request, $id)
    {
        try {
            $version = Version::findOrFail($id);

            if($version->subversions->first() != null)
                return response()->json(['message' => 'Esta versão contém subversões, portanto não é possivel excluir'], 400);

            $version->delete();

            return response()->json(['message' => 'Deletado com sucesso'], 200);

        } catch (\Throwable $th) {
            return response()->json(["error" => $th->getMessage()], 500);
        }
    }

    public function photos(Request $request, $id)
    {
        $version = Version::find($id);

        if($version == null)
            return response()->json(["error" => "Versão não encontrado."], 400);

        foreach($version->photos as $photo)
        {
            $photo->url();
        }

        return response()->json($version->photos, 200);
    }

    public function upload(Request $request, $id)
    {
        $version = Version::find($id);

        if($version == null)
            return response()->json(["error" => "Versão não encontrado."], 400);

        $photoService = new PhotoServiceProvider();
        $photos = $photoService->upload($request, $version);

        foreach($photos as $photo)
        {
            $photo->url();
        }

        return response()->json($photos, 200);
    }

    private function filter($request)
    {
        $pagination = --$request->pagination ?? 0;
        $limit = $request->limit ?? 10;
        $order = $request->order ?? "asc";
        $offset = $pagination * $limit;

        $car = $request->car ?? null;
        $year = $request->year ?? null;

        // BUSCA TODOS
        if($car == null)
            return Version::skip($offset * $limit)->take($limit)->orderBy('name', $order)->get();

        // BUSCA TODOS DO MODELO ESPECIFICO
        if($year == null)
            return Version::where(['car_id' => $car])->skip($offset * $limit)->take($limit)->orderBy('name', $order)->get();

        // BUSCA TODOS DO MODELO E ANO SELECIONADO
        return Version::whereHas('versionYear', function (Builder $query) use($year) {
            $query->where('year', $year);
        })->where(['car_id' => $car])->skip($offset * $limit)->take($limit)->orderBy('name', $order)->get();

        $query = Version::query();

        if($year != null){
            $query->whereHas('versionYear', function (Builder $query) use($year) {
                $query->where('year', $year);
            });
        }

        if($car != null)
            $query->where(['car_id' => $car]);

        return $query->skip($offset * $limit)->take($limit)->orderBy('name', $order)->get();


    }

}
