<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\{Subversion, Version, Brand, Car};

class SiteController extends Controller
{
    public function breadcrumb($type, $id){
        $breadcrumb = [];
        $match = False;
        if($type == "subversion"){
            $match = True;
            $subversion = Subversion::findOrFail($id);
            $breadcrumb[] = ["name" => $subversion->name, "url" => "/subversion/detail?subversion_id={$subversion->id}"];
        }
        if($match or $type == "version"){
            $idn = $match ? $subversion->version_id : $id;
            $match = True;
            $version = Version::findOrFail($idn);
            $breadcrumb[] = ["name" => $version->name, "url" => "/version/detail?version={$version->id}"];
        }
        if($match or $type == "car"){
            $idn = $match ? $version->car_id : $id;
            $match = True;
            $car = Car::findOrFail($idn);
            $breadcrumb[] = ["name" => $car->name, "url" => "/version?car={$car->id}"];
        }
        if($match or $type == "brand"){
            $idn = $match ? $car->brand_id : $id;
            $brand = Brand::findOrFail($idn);
            $breadcrumb[] = ["name" => $brand->name, "url" => "/car?brand={$brand->id}"];
        }
        $breadcrumb[] = ["name" => "Marcas", "url" => "/brand"];
        $breadcrumb = array_reverse($breadcrumb);
        return response()->json($breadcrumb, 200);
    }
}
