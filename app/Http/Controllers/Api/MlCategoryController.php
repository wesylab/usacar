<?php


namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MlCategory;
use App\Providers\MlServiceProvider;
use App\Http\Resources\MlCategory as MlCategoryResource;

class MlCategoryController extends Controller
{

    public function autocomplete(Request $request)
    {
        try{
            $ml_categories = MlCategory::where('name', 'like', "%{$request->text}%")->take(500)->orderBy('id', 'asc')->get();
            return response()->json(MlCategoryResource::collection($ml_categories), 201);
        } catch (\Throwable $th) {
            return response()->json(["error" => $th->getMessage()], 500);
        }

    }

}
