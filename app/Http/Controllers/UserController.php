<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{ User };

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    
    public function index(Request $request)
    {
        $data = [
            'title' => 'Usuários'
        ];

        $request->user()->authorizeRoles(['master']);
        return view('master.user.index', $data);
    }    

}
