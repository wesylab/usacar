<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{ Version };

class VersionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**Versões
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    
    public function index(Request $request)
    {
        $data = [
            'title' => ''
        ];

        $request->user()->authorizeRoles(['master']);
        return view('master.version.index', $data);
    }
    
    public function detail(Request $request)
    {
        $data = [
            'title' => ''
        ];

        $request->user()->authorizeRoles(['master']);
        return view('master.version.detail', $data);
    }

}
