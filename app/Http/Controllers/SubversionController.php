<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{ Subversion, Version };
use App\Http\Controllers\Controller;

class SubversionController extends Controller
{

    public function index(Request $request)
    {
        $data = [
            'title' => ''
        ]; 

        $request->user()->authorizeRoles(['master']);
        return view('master.subversion.index', $data);
    }
    
    public function detail(Request $request)
    {
        $data = [
            'title' => ''
        ];

        $request->user()->authorizeRoles(['master']);
        return view('master.subversion.detail', $data);
    }
}
