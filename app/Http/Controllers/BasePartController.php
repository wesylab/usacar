<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{ BasePart };
use App\Http\Controllers\Controller;

class BasePartController extends Controller
{

    public function index(Request $request)
    {
        $data = [
            'title' => ''
        ]; 

        $request->user()->authorizeRoles(['master']);
        return view('master.basepart.index', $data);
    }
    
    public function detail(Request $request)
    {
        $data = [
            'title' => ''
        ];

        $request->user()->authorizeRoles(['master']);
        return view('master.basepart.detail', $data);
    }
}
