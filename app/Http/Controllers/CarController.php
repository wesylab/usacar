<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{ Car, Brand };

class CarController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    
    public function index(Request $request)
    {
        $data = [
            'title' => ''
        ];

        $request->user()->authorizeRoles(['master']);
        return view('master.car.index', $data);
    }    

}
