<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Providers\FileServiceProvider;
use App\Models\Brand;

class BrandController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    
    public function index(Request $request)
    {
        $data = [
            'title' => ''
        ];

        $request->user()->authorizeRoles(['master']);
        return view('master.brand.index', $data);
    }

    public function cars(Request $request, $brand = null, $car = null)
    {
        if($car != null){
            return view('master.version.index');
        }
        if($brand != null){
            return view('master.car.index');
        }
        redirect('/brand');
    }

}
