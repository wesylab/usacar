<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\{ Photo, File, Version, VersionYear };

/**
 * @property int $id
 * @property string $name
 */
class Car extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'cars';

    /**
     * @var array
     */
    protected $fillable = ['name'];

    public function versions() 
    {
        return $this->hasMany('App\Models\Version');
    }

    public function photos() 
    {
        return $this->hasMany('App\Models\Photo');
    }

    public function brand()
    {
        return $this->belongsTo('App\Models\Brand');
    }

    public function cover()
    {
        $cover = Photo::where(['car_id' => $this->id])->first();
        if($cover != null){
            if($cover->file != null){
                $this->cover = $cover->file->url();
            }
        }else{
            $this->cover = "https://cdn4.iconfinder.com/data/icons/car-silhouettes/1000/sedan-512.png";
        }
    }

    public function yearsVersion()
    {
        $year_collection = collect();

        foreach($this->versions as $version)
        {
            foreach($version->versionYear as $version_year)
            {
                if(!$year_collection->search($version_year->year))
                    $year_collection->push($version_year->year);
            }
        }

        $this->yearsVersion = $year_collection->unique()->sort()->values()->toArray();
    }

}
