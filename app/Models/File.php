<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 */
class File extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'files';

    /**
     * @var array
     */
    protected $fillable = ['name', 'path', 'size'];

    public function dir()
    {
        return "{$this->path}{$this->name}";
    }

    public function url()
    {
        return url('/')."/{$this->path}".'/'."{$this->name}";
    }

}
