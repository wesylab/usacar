<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Freight extends Model
{

    protected $table = 'freight';

    protected $guarded = [];

    public $timestamps = false;

    public function base_part()
    {
        $this->belongsTo('BasePart');
    }

}
