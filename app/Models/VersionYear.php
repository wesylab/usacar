<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $year
 */
class VersionYear extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'version_years';

    /**
     * @var array
     */
    protected $fillable = ['year'];

    public function version()
    {
        return $this->belongsTo('App\Models\Version');
    }

}
