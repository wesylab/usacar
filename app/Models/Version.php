<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\VersionYear;

/**
 * @property int $id
 * @property string $name
 */
class Version extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'versions';

    /**
     * @var array
     */
    protected $fillable = ['name', 'year'];

    public function versionYear()
    {
        return $this->hasMany('App\Models\VersionYear');
    }

    public function photos()
    {
        return $this->hasMany('App\Models\Photo');
    }

    public function car()
    {
        return $this->belongsTo('App\Models\Car');
    }

    public function subversions(){
        return $this->hasMany('App\Models\Subversion');
    }

    public function cover()
    {
        $cover = Photo::where(['version_id' => $this->id])->first();
        if($cover != null){
            if($cover->file != null){
                $this->cover = $cover->file->url();
            }
        }else{
            $this->cover = "https://cdn4.iconfinder.com/data/icons/car-silhouettes/1000/sedan-512.png";
        }
    }

    public function years()
    {
        $years = $this->versionYear();

        $min = $years->min('year');
        $max = $years->max('year');

        $this->years = [$min, $max];
    }

    public function saveYears($years)
    {
        if(isset($years[0]) && isset($years[1]))
            $this->buildYears($years[0], $years[1]);
    }

    private function buildYears($start, $end)
    {
        $this->removeYears();

        for($i = $start; $i <= $end; $i++)
        {
            if(!empty($this->validateYear($i)))
                continue;

            $year = new VersionYear;
            $year->year = $i;
            $year->version()->associate($this);
            $year->save();
        }
    }

    private function removeYears()
    {
        VersionYear::where('version_id', $this->id)->delete();
    }

    private function validateYear($year)
    {
        return VersionYear::where(['year' => $year, 'version_id' => $this->id])->first();
    }

}
