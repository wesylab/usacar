<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $year
 */
class SubversionYear extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'subversion_years';

    /**
     * @var array
     */
    protected $fillable = ['year'];

    public $timestamps = false;

    public function subversion()
    {
        return $this->belongsTo('App\Models\Subversion');
    }

}
