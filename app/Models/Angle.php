<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 */
class Angle extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'angles';

    /**
     * @var array
     */
    protected $fillable = [];

    public function photos()
    {
        return $this->hasMany('App\Models\Photos');
    }

}
