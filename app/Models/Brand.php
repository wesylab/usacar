<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 */
class Brand extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'brands';

    /**
     * @var array
     */
    protected $fillable = ['name'];

    public function logo()
    {
        $this->logo = null;
        if($this->file != null)
            $this->logo = $this->file->url();
    }
    
    public function cars() 
    {
        return $this->hasMany('App\Models\Car');
    }

    public function file()
    {
        return $this->belongsTo('App\Models\File');
    }

}
