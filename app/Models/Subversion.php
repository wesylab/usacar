<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Providers\PhotoServiceProvider;
use App\Models\{ Photo };

class Subversion extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'subversions';

    public function version(){
        return $this->belongsTo('App\Models\Version');
    }

    public function photos()
    {
        return $this->hasMany('App\Models\Photo');
    }

    public function subversionYears()
    {
        return $this->hasMany('App\Models\SubversionYear');
    }

    public function cover()
    {
        $cover = Photo::where(['subversion_id' => $this->id])->first();
        if($cover != null and $cover->file != null){
            return $cover->file->url();
        }else{
            return "https://cdn4.iconfinder.com/data/icons/car-silhouettes/1000/sedan-512.png";
        }
    }

    public function years()
    {
        $years = $this->subversionYears();

        $min = $years->min('year');
        $max = $years->max('year');

        $this->years = [$min, $max];
    }

    public function saveYears($years)
    {
        if(isset($years[0]) && isset($years[1]))
            $this->buildYears($years[0], $years[1]);
    }

    private function buildYears($start, $end)
    {
        $this->removeYears();

        for($i = $start; $i <= $end; $i++)
        {
            if(!empty($this->validateYear($i)))
                continue;

            $year = new SubversionYear;
            $year->year = $i;
            $year->subversion()->associate($this);
            $year->save();
        }
    }

    private function removeYears()
    {
        SubversionYear::where('subversion_id', $this->id)->delete();
    }

    private function validateYear($year)
    {
        return SubversionYear::where(['year' => $year, 'subversion_id' => $this->id])->first();
    }

}
