<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MlCategory extends Model
{

    protected $table = 'ml_categories';

    protected $guarded = [];

    public function base_parts()
    {
        return $this->hasMany('BasePart');
    }

}
