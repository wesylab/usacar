<?php

namespace App\Models;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use App\Models\{ Freight, MlCategory };

class BasePart extends Model
{

    protected $table = 'base_parts';

    protected $guarded = [];

    public function ml_category()
    {
        return $this->belongsTo('App\Models\MlCategory');
    }

    public function freight()
    {
        return $this->hasMany('App\Models\Freight');
    }

    public function saveFreight(Request $request)
    {
        if(!isset($request->freights) or $request->freights == null)
        {
            return;
        }
        $this->freight()->delete();

        foreach ($request->freights as $freight)
        {
            if(!$freight['region_id'] || !$freight['amount'])
                continue;

            $this->freight()->create([
                'amount' => $freight['amount'],
                'region_id' => $freight['region_id']
            ]);
        }

    }

}
