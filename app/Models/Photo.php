<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 */
class Photo extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'photos';

    /**
     * @var array
     */
    protected $fillable = [];
    
    public function car()
    {
        return $this->belongsTo('App\Models\Car');
    }

    public function file()
    {
        return $this->belongsTo('App\Models\File');
    }

    public function angle()
    {
        return $this->belongsTo('App\Models\Angle');
    }

    public function version()
    {
        return $this->belongsTo('App\Models\Version');
    }

    public function  subversion(){
        return $this->belongsTo('App\Models\Subversion');
    }

    public function getUrl()
    {
        if($this->file != null){
            return $this->file->url();
        }
    }

    public function url()
    {
        $this->url = null;
        if($this->file != null){
            $this->url = $this->file->url();
        }
    }

}
