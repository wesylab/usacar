<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\MlCategory;
use GuzzleHttp\Client;

class MlServiceProvider extends ServiceProvider
{

    public function __construct()
    {
        $this->client = new Client();
    }

    private function req($url, $method = "GET", $data = [])
    {
        $response = $this->client->request($method, $url, $data);
        return $response->getBody();
    }

    public function categories()
    {

        $categories = json_decode($this->req('https://api.mercadolibre.com/categories/MLB5672'));

        $parent = $this->saveMlCategory([
            'ml_code' => $categories->id,
            'name' => $categories->name
        ]);

        $this->saveChildren($parent);

    }

    private function saveChildren($parent)
    {
        $category = json_decode($this->req("https://api.mercadolibre.com/categories/{$parent->ml_code}"));
        foreach ($category->children_categories as $child)
        {
            $this->saveChildren($this->saveMlCategory([
                'ml_code' => $child->id,
                'name' => $child->name,
                'ml_category_id' => $parent->id
            ]));
        }
    }

    private function saveMlCategory($data)
    {
        if(!$data['ml_code'] || !$data['name'])
            return false;

        $ml_category = MlCategory::where([
            'ml_code' => $data['ml_code'],
            'name' => $data['name']
        ])->first();

        if($ml_category == null)
            $ml_category = MlCategory::create($data);

        return $ml_category;

    }

}
