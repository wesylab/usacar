<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;
use Illuminate\Http\Request;
use App\Models\File;

class FileServiceProvider extends ServiceProvider
{

    private $path = ''; 
    private $name = ''; 
    private $extension = '';
    private $size = 0;
    private $filename  = '';

    public function __construct()
    {

    }

    public function delete(File $file)
    {
        if(!is_dir($file->dir())){
            if(file_exists($file->dir()))
                unlink($file->dir());
        }
        $file->delete();
    }

    private function _validate(Request $request)
    {
        if(!$request->hasFile('image'))
            return "O arquivo é obrigatório.";

        if(!$request->file('image')->isValid())
            return "Ocorreu algum erro no envio do arquivo.";

        return true;
    }

    private function _save()
    {
        $file = new File();
        $file->name = $this->filename;
        $file->path = $this->path;
        $file->size = $this->size;
        $file->save();
        return $file;
    }

    public function upload(Request $request, $path = 'storage/')
    {
        $response = ['status' => 400, 'message' => '', 'file' => null];

        $this->path = $path;

        $validate = $this->_validate($request);
        if($validate !== true){
            $response['message'] = $validate;
            return $response;
        }
        $file = $this->run($request->file('image'));

        if($file != null){
            $response['message'] = "Upload realizado com sucesso.";
            $response['file'] = $file;
            $response['status'] = 200;
        }else{
            $response['message'] = "Ocorreu algum erro ao salvar o arquivo.";
            $response['status'] = 500;
        }
        return $response;

    }

    public function _multiple($file, $path) 
    {
        $this->path = $path;
        return $this->run($file);
    }

    private function run($file)
    {
        $this->name = uniqid(date('HisYmd'));     
        $this->_verifyPath();

        $this->size = $file->getSize();   
        $this->extension = $file->extension();
        $this->_buildFilename();

        if($file->move($this->path, $this->filename))
            return $this->_save();
        
        return null;
    }

    private function _buildFilename()
    {
        $this->filename = "{$this->name}.{$this->extension}";
    }

    private function _verifyPath()
    {
        if(!is_dir($this->path))
            mkdir($this->path, 0777, true);
    }

    private function _getBase64Type($str) {
        $pos  = strpos($str, ';');
        $type = explode(':', substr($str, 0, $pos))[1];
        $type = str_replace("image/", "", $type);
        $this->extension = $type;
    }

    // private function _image(Request $request)
    // {
    //     $this->size = $request->image->getSize();   
    //     $this->extension = $request->image->extension();

    //     $this->_buildFilename();

    //     return $request->image->move($this->path, $this->filename);
    // }

    // private function _base64(Request $request)
    // {
    //     $image = $request->image;
    //     $this->_getBase64Type($image);
    //     $this->_buildFilename();

    //     $image = explode(',', $image)[1];
    //     $image = base64_decode($image);

    //     return file_put_contents("{$this->path}{$this->filename}", $image);
    // }

}
