<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;
use Illuminate\Http\Request;
use App\Models\{ File, Photo, Car, Version, Subversion };

class PhotoServiceProvider extends ServiceProvider
{

    public function __construct()
    {
        $this->path = "";
    }

    public function delete(Photo $photo)
    {
        $fileService = new FileServiceProvider();
        $fileService->delete($photo->file);
    }

    public function upload(Request $request, $obj)
    {

        $type = null;

        if($obj instanceof Car){
            $this->path = "storage/brands/{$obj->brand_id}/{$obj->id}/";
            $type = "car";
        }elseif($obj instanceof Version){
            $this->path = "storage/brands/{$obj->car->brand_id}/{$obj->car_id}/{$obj->id}/";
            $type = "version";
        }elseif($obj instanceof Subversion){
            $this->path = "storage/brands/{$obj->version->car->brand_id}/{$obj->version->car_id}/{$obj->version->id}/{$obj->id}";
            $type = "subversion";
        }
        if($type == null)
            return [];
     
        $list_photos = [];

        foreach($request->image as $file)
        {
            $photo = new Photo();

            if($type == 'car')
                $photo->car()->associate($obj);
            elseif($type == 'version')
                $photo->version()->associate($obj);
            elseif($type == 'subversion'){
                $photo->subversion()->associate($obj);
            }

            $fileService = new FileServiceProvider();
            $file = $fileService->_multiple($file, $this->path);

            $photo->file()->associate($file);
            $photo->save();

            $list_photos[] = $photo;
            $photo = null;
        }

        return $list_photos;
    }

}