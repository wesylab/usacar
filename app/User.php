<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function authorizeRoles($roles)
    {  
        if(is_array($roles)) 
        {      
            return $this->hasAnyRole($roles) || 
                   abort(401, 'This action is unauthorized.');  
        }  
        return $this->hasRole($roles) || 
               abort(401, 'This action is unauthorized.');
    }

    public function hasAnyRole($roles)
    {  
        return null !== $this->roles()->whereIn('name', $roles)->first();
    }
    
    public function hasRole($role)
    {  
        return null !== $this->roles()->where('name', $role)->first();
    }

    public function newPassword($pass = null)
    {
        $hash = $pass;
        if($pass == null)
        {
            $pass = $this->_randomPassword();
            $hash = md5($pass);
        }

        $this->password = password_hash($hash, PASSWORD_DEFAULT);
        return $pass;
    }

    private function _randomPassword() {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    public function findForPassport($identifier)
    {
        return $this->orWhere(‘email’, $identifier)->first();
    }

}
