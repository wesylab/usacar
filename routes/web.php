<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    // return view('welcome');
    return redirect('/home');
});

// Auth::routes();
Auth::routes(['register' => true]);

Route::get('/home', 'HomeController@index')->name('home');

/* ----------------------------- VIEWS ------------------------------------------ */
Route::get('/brand', 'BrandController@index');
Route::get('/car', 'CarController@index');
Route::get('/version', 'VersionController@index');
Route::get('/version/detail', 'VersionController@detail');
Route::get('/subversion/detail', 'SubversionController@detail');
Route::get('/user', 'UserController@index');
Route::get('/basepart', 'BasePartController@index');
Route::get('login', 'Auth\LoginController@login2')->name('login');



/* ----------------------------- REST ------------------------------------------ */
Route::group(array('prefix' => '/iapi'), function(){

    Route::group(array('prefix' => '/brand'), function(){
        Route::get('/', 'Api\BrandController@all');
        Route::get('/{id}', 'Api\BrandController@get');
        Route::post('/', 'Api\BrandController@insert');
        Route::post('/{id}', 'Api\BrandController@update');
        Route::delete('/{id}', 'Api\BrandController@delete');
    });

    Route::group(array('prefix' => '/car'), function(){
        Route::get('/', 'Api\CarController@all');
        Route::get('/{id}', 'Api\CarController@get');
        Route::post('/', 'Api\CarController@insert');
        Route::post('/{id}', 'Api\CarController@update');
        Route::delete('/{id}', 'Api\CarController@delete');
        Route::get('/{id}/photos', 'Api\CarController@photos');
        Route::get('/{id}/yearsVersion', 'Api\CarController@yearsVersion');
        Route::post('/{id}/upload', 'Api\CarController@upload');
    });

    Route::group(array('prefix' => '/version'), function(){
        Route::get('/', 'Api\VersionController@all');
        Route::get('/{id}', 'Api\VersionController@get');
        Route::post('/', 'Api\VersionController@insert');
        Route::post('/{id}', 'Api\VersionController@update');
        Route::delete('/{id}', 'Api\VersionController@delete');
        Route::get('/{id}/photos', 'Api\VersionController@photos');
        Route::post('/{id}/upload', 'Api\VersionController@upload');
    });

    Route::group(array('prefix' => '/user'), function(){
        Route::get('/', 'Api\UserController@all');
        Route::get('/{id}', 'Api\UserController@get');
        Route::post('/', 'Api\UserController@insert');
        Route::post('/{id}', 'Api\UserController@update');
        Route::delete('/{id}', 'Api\UserController@delete');
    });

    Route::group(array('prefix' => '/photo'), function(){
        Route::delete('/{id}', 'Api\PhotoController@delete');
    });

    Route::group(array('prefix' => '/subversion'), function(){
        Route::get('/', 'Api\SubversionController@all');
        Route::get('/{id}', 'Api\SubversionController@get');
        Route::post('/', 'Api\SubversionController@insert');
        Route::post('/{id}', 'Api\SubversionController@update');
        Route::delete('/{id}', 'Api\SubversionController@delete');
        Route::post('/{id}/upload', 'Api\SubversionController@upload');
        Route::get('/{id}/photos', 'Api\SubversionController@photos');
    });

    Route::group(array('prefix' => '/site'), function(){
        Route::get('/breadcrumb/{type}/{id}', 'Api\SiteController@breadcrumb');
    });

    Route::group(array('prefix' => '/base-part'), function(){
        Route::get('/', 'Api\BasePartController@all');
        Route::get('/{id}', 'Api\BasePartController@get');
        Route::post('/', 'Api\BasePartController@insert');
        Route::post('/{id}', 'Api\BasePartController@update');
        Route::delete('/{id}', 'Api\BasePartController@delete');
    });

    Route::group(array('prefix' => '/ml-category'), function(){
        Route::get('/autocomplete', 'Api\MlCategoryController@autocomplete');
    });

});
