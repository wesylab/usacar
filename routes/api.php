<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'Auth\AuthController@login');
    Route::post('signup', 'Auth\AuthController@signup');

    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'Auth\AuthController@logout');
        Route::get('user', 'Auth\AuthController@user');
    });
});
Route::group(['middleware' => ['auth:api']], function () {


    Route::group(array('prefix' => '/version'), function(){
        Route::get('/', 'Api\VersionController@all');
        Route::get('/{id}', 'Api\VersionController@get');
        Route::post('/', 'Api\VersionController@insert');
        Route::post('/{id}', 'Api\VersionController@update');
        Route::delete('/{id}', 'Api\VersionController@delete');
        Route::get('/{id}/photos', 'Api\VersionController@photos');
        Route::post('/{id}/upload', 'Api\VersionController@upload');
    });

        Route::group(array('prefix' => '/subversion'), function(){
            Route::get('/', 'Api\SubversionController@all');
            Route::get('/{id}', 'Api\SubversionController@get');
            Route::post('/', 'Api\SubversionController@insert');
            Route::put('/{id}', 'Api\SubversionController@update');
            Route::delete('/{id}', 'Api\SubversionController@delete');
            Route::post('/{id}/upload', 'Api\SubversionController@upload');
            Route::get('/{id}/photos', 'Api\SubversionController@photos');
        });

        Route::group(array('prefix' => '/site'), function(){
            Route::get('/breadcrumb/{type}/{id}', 'Api\SiteController@breadcrumb');
        });

    Route::group(array('prefix' => '/base-part'), function(){
        Route::get('/', 'Api\BasePartController@all');
        Route::get('/{id}', 'Api\BasePartController@get');
        Route::post('/', 'Api\BasePartController@insert');
        Route::put('/{id}', 'Api\BasePartController@update');
        Route::delete('/{id}', 'Api\BasePartController@delete');
    });

    Route::group(array('prefix' => '/ml-category'), function(){
        Route::get('/autocomplete', 'Api\MlCategoryController@autocomplete');
    });

});

Route::get('/teste', 'Api\BasePartController@teste');

// Route::group(array('prefix' => '/brand'), function(){
//     Route::get('/', 'Api\BrandController@all');
//     Route::post('/', 'Api\BrandController@insert');
// });
