
var urlParams;
(window.onpopstate = function () {
    var match,
        pl     = /\+/g,  // Regex for replacing addition symbol with a space
        search = /([^&=]+)=?([^&]*)/g,
        decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
        query  = window.location.search.substring(1);

    urlParams = {};
    while (match = search.exec(query))  
       urlParams[decode(match[1])] = decode(match[2]);
})();

function objToFormData(obj)
{
    let formData = new FormData();
    buildFormData(formData, obj);
    return formData;
}

function currentYear()
{
  return new Date().getFullYear();
}

function buildFormData(formData, data, parentKey) 
{
    if (data && typeof data === 'object' && !(data instanceof Date) && !(data instanceof File)) {
      Object.keys(data).forEach(key => {
        buildFormData(formData, data[key], parentKey ? `${parentKey}[${key}]` : key);
      });
    } else {
      const value = data == null ? '' : data;
  
      formData.append(parentKey, value);
    }
}

function closeModal(query)
{
  $(query).modal("hide");
  $('body').removeClass('modal-open');
  $('.modal-backdrop').remove();
}

function vueToast(message, type)
{
  Vue.$toast.open({
    'message': message,
    'type': type,
    'position': 'top-right',
    'duration': 3000 
  });
}
