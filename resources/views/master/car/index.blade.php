@extends('layouts.app')

@section('content')

<brand-detail></brand-detail>
<cars-component></cars-component>
<div class="container">
    <div class="row justify-content-center">
        <new-car></new-car>
        <gallery-car></gallery-car>
    </div>
</div>

@endsection