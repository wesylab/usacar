@extends('layouts.app')

@section('content')

<car-detail></car-detail>
<versions-component></versions-component>
<div class="container">
    <div class="row justify-content-center">
        <new-version></new-version>
        <gallery-version></gallery-version>
    </div>
</div>

@endsection