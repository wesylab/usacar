@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                {{ Form::open(array('url' => 'brand', 'files' => true)) }}
                    {{ Form::text('name') }}
                    {{ Form::file('image') }}
                    {{ Form::submit('Click Me!') }}
                {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection