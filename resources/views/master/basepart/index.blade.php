
@extends('layouts.app')

@section('content')


<div class="container">
    <new-basepart ref="modalBasepart"></new-basepart>
    <div class="row justify-content-center">
        <basepart-component></basepart-component>
    </div>
</div>

@endsection