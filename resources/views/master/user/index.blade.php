@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <users-component></users-component>
        <new-user></new-user>
    </div>
</div>

@endsection