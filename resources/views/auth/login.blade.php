@extends('layouts.app')
<link rel="stylesheet" type="text/css" href="{{ asset('css/login.css') }}" />
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.indigo-pink.min.css">
<script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header text-center"><img id="img-logo" src="http://www.usacarautopecas.com.br/hp/img/top_logo.png"></div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <!-- <div class="row">
                            <label for="email" class="col-md-12 text-md-left">{{ __('Email:') }}</label>

                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div> -->
                        <div class="row">
                            <div class="col-md-12 col-lg-12 col-sm-6">
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                <label class="mdl-textfield__label" for="email">{{ __('Email:') }}</label>
                                <input class="mdl-textfield__input @error('email') is-invalid @enderror" id="email" type="email" name="email" value="{{ old('email') }}" autocomplete="email" autofocus>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 col-lg-12 col-sm-6">
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                <label class="mdl-textfield__label" for="password">{{ __('Senha:') }}</label>
                                <input class="mdl-textfield__input @error('password') is-invalid @enderror" id="password" type="password" name="password" autocomplete="password" autofocus>
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            </div>
                        </div>

                        <!-- <div class="form-group row">
                            <label for="password" class="col-md-12 text-md-left">{{ __('Senha:') }}</label>

                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div> -->
                        

                  <!--      <div classEntrar
                            <div cEntrarember">
                                <lEntrarox mdl-js-ripple-effect" for="remember">
                                  Entrar class="mdl-checkbox__input" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                  Entrar __('Lembrar-me') }}</span>
                                </Entrar
                            </div>Entrar
                        </div>
-->
                        <!-- <div class="form-group row">
                            <div class="col-md-6">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                    <label class="form-check-label" id="label-remember" for="remember">{{ __('Lembrar-me') }}</label>
                                </div>
                            </div>
                        </div> -->

                        <div class="row">
                            <div class="col-md-12 col-lg-12 col-sm-6 remember">
                            <button class=" btn-logar mdl-button mdl-js-button mdl-button--raised mdl-button--colored col-md-12">
                                   Entrar
                            </button>
                        </div>
                        <!-- <div class="form-group row mb-0">
                            <div class="col-md-12"> -->
                                <!-- <button type="submit" class="col-md-12 btn btn-light btn-logar">
                                    {{ __('Entrar') }}
                                </button> -->
                                
                            <!-- </div>
                        </div> -->
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
