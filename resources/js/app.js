require('./bootstrap');

window.Vue = require('vue');

import Vue from 'vue';

import vuetify from '../../src/plugins/vuetify.js';

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

Vue.component('header-component', require('./components/Default/HeaderComponent.vue').default);

Vue.component('brands-component', require('./components/Brand/BrandsComponent.vue').default);
Vue.component('new-brand', require('./components/Brand/New.vue').default);
Vue.component('brand-detail', require('./components/Brand/Detail.vue').default);

Vue.component('cars-component', require('./components/Car/CarsComponent.vue').default);
Vue.component('new-car', require('./components/Car/New.vue').default);
Vue.component('gallery-car', require('./components/Car/Gallery.vue').default);
Vue.component('car-detail', require('./components/Car/Detail.vue').default);

Vue.component('versions-component', require('./components/Version/VersionsComponent.vue').default);
Vue.component('new-version', require('./components/Version/New.vue').default);
Vue.component('gallery-version', require('./components/Version/Gallery.vue').default);
Vue.component('version-detail', require('./components/Version/Detail.vue').default);

Vue.component('subversions-component', require('./components/Subversion/SubVersionsComponents.vue').default);
Vue.component('new-subversion', require('./components/Subversion/New.vue').default);
Vue.component('gallery-subversion', require('./components/Subversion/Gallery.vue').default);
Vue.component('subversion-detail', require('./components/Subversion/Detail.vue').default);

Vue.component('basepart-component', require('./components/BasePart/BasePartComponent.vue').default);
Vue.component('new-basepart', require('./components/BasePart/New.vue').default);

Vue.component('users-component', require('./components/User/UsersComponent.vue').default);
Vue.component('new-user', require('./components/User/New.vue').default);
Vue.component('login2', require('./components/Login/Login.vue').default);

window.onload = function () {

Vue.prototype.$eventHub = new Vue();

const app = new Vue({
    vuetify,
    el: '#app',
});
}

