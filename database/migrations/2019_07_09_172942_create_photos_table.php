<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('photos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger("file_id");
            $table->unsignedBigInteger("car_id")->nullable(true);
            $table->unsignedBigInteger("version_id")->nullable(true);
            $table->timestamps();
        });

        Schema::table('photos', function($table){
            $table->foreign("file_id")->references('id')->on('files')->onDelete('cascade');
            $table->foreign("car_id")->references('id')->on('cars')->onDelete('cascade');
            $table->foreign("version_id")->references('id')->on('versions')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('photos', function($table){
            $table->dropForeign(["file_id" ]);
            $table->dropForeign(["car_id" ]);
            $table->dropForeign(["version_id"]);
        });
        Schema::dropIfExists('photos');
    }
}
