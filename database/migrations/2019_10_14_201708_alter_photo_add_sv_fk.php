<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPhotoAddSvFk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('photos', function (Blueprint $table) {
            $table->unsignedBigInteger("subversion_id")->nullable(true);
        });

        Schema::table('photos', function($table){
            $table->foreign("subversion_id")->references('id')->on('subversions')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('photos', function($table){
            $table->dropForeign('photos_subversion_id_foreign');
            $table->dropColumn("subversion_id");
        });
    }
}
