<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBasePartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('base_parts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('description');
            $table->enum('car_region', ['Frontal', 'Lateral', 'Traseiro']);
            $table->boolean('ml_variation')->default(false);
            $table->text('technical_description_new')->nullable(true);
            $table->text('technical_description_old')->nullable(true);
            $table->string('clas_indaia_new')->nullable(true);
            $table->string('clas_indaia_old')->nullable(true);
            $table->unsignedBigInteger("ml_category_id")->nullable(true);
            $table->timestamps();
        });

        Schema::table('base_parts', function($table){
            $table->foreign("ml_category_id")->references('id')->on('ml_categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('base_parts');
    }
}
