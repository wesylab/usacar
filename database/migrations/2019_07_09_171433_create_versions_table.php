<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVersionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('versions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->year("year");
            $table->unsignedBigInteger("car_id");
            $table->timestamps();
        });

        Schema::table('versions', function($table){
            $table->foreign("car_id")->references('id')->on('cars')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('versions', function($table){
            $table->dropForeign(["car_id"]);
        });
        Schema::dropIfExists('versions');
    }
}
