<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVersionYearsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('version_years', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger("version_id");
            $table->year("year");
            $table->timestamps();
        });

        Schema::table('version_years', function($table){
            $table->foreign("version_id")->references('id')->on('versions')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('version_years', function($table){
            $table->dropForeign(["version_id"]);
        });
        Schema::dropIfExists('version_years');
    }
}
