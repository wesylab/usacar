<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubversionYearsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subversion_years', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger("subversion_id");
            $table->year("year");
        });

        Schema::table('subversion_years', function($table){
            $table->foreign("subversion_id")->references('id')->on('subversions')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subversion_years');
    }
}
