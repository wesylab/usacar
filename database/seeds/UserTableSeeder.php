<?php

use Illuminate\Database\Seeder;
use App\{ Role, User };

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $user = User::where(['email' => 'diogo@wesy.com.br'])->get();

        if($user)
            return;

        $role_master = Role::where('name', 'master')->first();    
        
        $manager = new User();
        $manager->name = 'Diogo';
        $manager->email = 'diogo@wesy.com.br';
        $manager->password = bcrypt('diogo123');
        $manager->save();
        $manager->roles()->attach($role_master);
    }
}
