<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AnglesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $angles = [
            'Dianteira',
            'Traseiro',
            'Lateral direito',
            'Lateral esquerdo'
        ];

        $insert_angles = [];

        foreach($angles as $key => $angle)
        {
            if(!DB::table('angles')->find(($key + 1)))
                $insert_angles[] = ['id' => ($key + 1), 'name' => $angle];
        }

        if(count($insert_angles) > 0)
            DB::table('angles')->insert($insert_angles);
    }
}
