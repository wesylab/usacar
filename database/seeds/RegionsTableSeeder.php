<?php

use Illuminate\Database\Seeder;

class RegionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $regions = [
            'Acre (AC) Capital',
            'Acre (AC) Interior',
            'Alagoas (AL) Capital',
            'Alagoas (AL) Interior',
            'Amapá (AP) Capital',
            'Amapá (AP) Interior',
            'Amazonas (AM) Capital',
            'Amazonas (AM) Interior',
            'Bahia (BA) Capital',
            'Bahia (BA) Interior',
            'Ceará (CE) Capital',
            'Ceará (CE) Interior',
            'Distrito Federal (DF) Capital',
            'Distrito Federal (DF) Interior',
            'Espírito Santo (ES) Capital',
            'Espírito Santo (ES) Interior',
            'Goiás (GO) Capital',
            'Goiás (GO) Interior',
            'Maranhão (MA) Capital',
            'Maranhão (MA) Interior',
            'Mato Grosso (MT) Capital',
            'Mato Grosso (MT) Interior',
            'Mato Grosso do Sul (MS) Capital',
            'Mato Grosso do Sul (MS) Interior',
            'Minas Gerais (MG) Capital',
            'Minas Gerais (MG) Interior',
            'Pará (PA) Capital',
            'Pará (PA) Interior',
            'Paraíba (PB) Capital',
            'Paraíba (PB) Interior',
            'Paraná (PR) Capital',
            'Paraná (PR) Interior',
            'Pernambuco (PE) Capital',
            'Pernambuco (PE) Interior',
            'Piauí (PI) Capital',
            'Piauí (PI) Interior',
            'Rio de Janeiro (RJ) Capital',
            'Rio de Janeiro (RJ) Interior',
            'Rio Grande do Norte (RN) Capital',
            'Rio Grande do Norte (RN) Interior',
            'Rio Grande do Sul (RS) Capital',
            'Rio Grande do Sul (RS) Interior',
            'Rondônia (RO) Capital',
            'Rondônia (RO) Interior',
            'Roraima (RR) Capital',
            'Roraima (RR) Interior',
            'Santa Catarina (SC) Capital',
            'Santa Catarina (SC) Interior',
            'São Paulo (SP) Capital',
            'São Paulo (SP) Interior',
            'Sergipe (SE) Capital',
            'Sergipe (SE) Interior',
            'Tocantins (TO) Capital',
            'Tocantins (TO) Interior'
        ];

        $insert_regions = [];

        foreach($regions as $key => $region)
        {
            if(!DB::table('regions')->find(($key + 1)))
                $insert_regions[] = ['id' => ($key + 1), 'name' => $region];
        }

        if(count($insert_regions) > 0)
            DB::table('regions')->insert($insert_regions);
    }
}
